#!/bin/bash
# Code from https://stackoverflow.com/questions/3430330/best-way-to-make-a-shell-script-daemon/29107686#29107686
DEBUG=false

# This part is for fun, if you consider shell scripts fun- and I do.
trap process_exit SIGUSR1

process_exit() {
    echo 'Got signal SIG_FAN_CONTROL_END: exit now'
    exit 0
}
# End of fun. Now on to the business end of things.

print_debug() {
    whatiam="$1"; tty="$2"
    [[ "$tty" != "not a tty" ]] && {
        echo "" >$tty
        echo "$whatiam, PID $$" >$tty
        ps -o pid,sess,pgid -p $$ >$tty
        tty >$tty
    }
}

me_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
me_FILE=$(basename $0)
cd /


#### CHILD HERE --------------------------------------------------------------------->
if [ "$1" = "child" ] ; then   # 2. We are the child. We need to fork again.
    shift; tty="$1"; shift
    $DEBUG && print_debug "*** CHILD, NEW SESSION, NEW PGID" "$tty"
    umask 0
    $me_DIR/$me_FILE XXrefork_daemonXX "$tty" "$@" </dev/null >/dev/null 2>/dev/null &
    $DEBUG && [[ "$tty" != "not a tty" ]] && echo "CHILD OUT" >$tty
    exit 0
fi

##### ENTRY POINT HERE -------------------------------------------------------------->
if [ "$1" != "XXrefork_daemonXX" ] ; then # 1. This is where the original call starts.
    existing_thread="$(ps aux | grep "/bin/bash $me_DIR/$me_FILE XXrefork_daemonXX" | grep -v grep)"
    if [ ! -z "$existing_thread" ]; then
        echo "$me_DIR/$me_FILE is running. The daemon is not started"
        exit 0
    fi
    tty=$(tty)
    $DEBUG && print_debug "*** PARENT" "$tty"
    setsid $me_DIR/$me_FILE child "$tty" "$@" &
    $DEBUG && [[ "$tty" != "not a tty" ]] && echo "PARENT OUT" >$tty
    exit 0
fi

##### RUNS AFTER CHILD FORKS (actually, on Linux, clone()s. See strace -------------->
                               # 3. We have been reforked. Go to work.

mkdir -p /tmp/daemons/fan_control/
exec >/tmp/daemons/fan_control/logfile
exec 2>/tmp//daemons/fan_control/errfile
exec 0</dev/null

echo "The pid is $$. Use the [kill -SIGUSR1 $$] command to end this daemon"

shift; tty="$1"; shift

$DEBUG && print_debug "*** DAEMON" "$tty"
                               # The real stuff goes here. To exit, see fun (above)
$DEBUG && [[ "$tty" != "not a tty" ]]  && echo NOT A REAL DAEMON. NOT RUNNING WHILE LOOP. >$tty

$DEBUG || {
    mode="Normal"
    CPU_limit_coeficient=1
    default_fan_speed_coeficient=30
    let fan_speed_coeficient=default_fan_speed_coeficient+1
    CPU_limit_coeficient_to_change=-1
    
    high_temp=60
while true; do
    hour=$(date +"%H:%M")
    
    timestamp=$(date)
    CPU1_temp=$(ipmitool sensor get "CPU1 Temp" | grep  "Reading" | grep -o ': [0-9]*' | grep -o '[0-9]*')
    CPU2_temp=$(ipmitool sensor get "CPU2 Temp" | grep  "Reading" | grep -o ': [0-9]*' | grep -o '[0-9]*')
    FAN1_speed=$(ipmitool sensor get "FAN2" | grep  "Reading" | grep -o ': [0-9]*' | grep -o '[0-9]*')
    CPU_temp=$(( CPU1_temp > CPU2_temp ? CPU1_temp : CPU2_temp ))
    if (( $CPU_temp > 59 )); then
        if (( $CPU_temp > $high_temp )); then
            high_temp=$CPU_temp
            # 60 -> default_fan_speed_coeficient & 90 -> default_fan_speed_coeficient + 70
            let tmp=high_temp-40
            let fan_speed_coeficient=tmp*tmp/30-12+default_fan_speed_coeficient
            fan_speed_coeficient_hex=$(printf '%x\n' $fan_speed_coeficient)
            ipmitool raw 0x30 0x70 0x66 0x01 0x00 0x$fan_speed_coeficient_hex
            echo "[$timestamp]: CPU temperature (max) increased: $CPU_temp. Set new Fan speed: $fan_speed_coeficient."
            
            if (( $high_temp > 85 )); then
                 CPU_limit_coeficient_to_change=$(echo "$CPU_limit_coeficient * 0.9"| bc -l | awk '{printf "%f", $0}')
            fi
            
        else
            let CPU_temp_plus3=CPU_temp+3
            if (( $CPU_temp_plus3 < $high_temp )); then
                high_temp=$CPU_temp
                let tmp=high_temp-40
                let fan_speed_coeficient=tmp*tmp/30-12+default_fan_speed_coeficient
                fan_speed_coeficient_hex=$(printf '%x\n' $fan_speed_coeficient)
                ipmitool raw 0x30 0x70 0x66 0x01 0x00 0x$fan_speed_coeficient_hex
                echo "[$timestamp]: CPU temperature (max) decreased: $CPU_temp. Set new Fan speed: $fan_speed_coeficient."
            fi
        fi
    else
        # echo "[$timestamp]: CPU temperature (max): $CPU_temp. Fan speed $FAN1_speed"
        if (( $FAN1_speed > 1900 )); then
            high_temp=$CPU_temp
            let fan_speed_coeficient=default_fan_speed_coeficient
            fan_speed_coeficient_hex=$(printf '%x\n' $fan_speed_coeficient)
            ipmitool raw 0x30 0x70 0x66 0x01 0x00 0x$fan_speed_coeficient_hex
            echo "[$timestamp]: CPU temperature (max): $CPU_temp. Fan speed $FAN1_speed is too high. Reset Fan speed: $fan_speed_coeficient."
        fi
        if (( $fan_speed_coeficient > default_fan_speed_coeficient )); then
            high_temp=$CPU_temp
            let fan_speed_coeficient=default_fan_speed_coeficient
            fan_speed_coeficient_hex=$(printf '%x\n' $fan_speed_coeficient)
            ipmitool raw 0x30 0x70 0x66 0x01 0x00 0x$fan_speed_coeficient_hex
            echo "[$timestamp]: CPU temperature (max): $CPU_temp is cool. Set new Fan speed: $fan_speed_coeficient."
        fi
    fi
    
    machineList=$(qm list | awk '{print $1}' | grep -o [0-9]*)
    if [ "$hour" = "08:00" ]; then
        CPU_limit_coeficient_to_change=2
    fi
    if [ "$hour" = "22:30" ]; then
        CPU_limit_coeficient_to_change=0.1
    fi
    if (( $(echo "$CPU_limit_coeficient_to_change + 1 != 0" | bc -l) )); then
        CPU_limit_coeficient=$CPU_limit_coeficient_to_change 
        CPU_limit_coeficient_to_change=-1
        echo "[$timestamp]: CPU temperature (max): $CPU_temp. Mode: $mode. Fan speed: $fan_speed_coeficient."
        for value in $machineList
        do
            cpulimit=$(qm config $value | grep cpulimit | grep -o '[0-9.]*')
            socket=$(qm config $value | grep socket | grep -o '[0-9.]*')
            cores=$(qm config $value | grep cores | grep -o '[0-9.]*')
            
            newCpuLimit=$(echo "$socket * $cores * $CPU_limit_coeficient" | bc -l | awk '{printf "%f", $0}')
            echo "$value -- Old limit: $cpulimit. New limit: $newCpuLimit."
            if (( $(echo "$cpulimit - $newCpuLimit != 0" | bc -l) )); then
                qm set $value --cpulimit $newCpuLimit
                checkNewCpuLimit=$(qm config $value | grep cpulimit | grep -o '[0-9.]*')
                echo "Set $value a new limit: CPU limit: $checkNewCpuLimit"
            fi
        done
    fi
    
    
   
    sleep 3
done
}

$DEBUG && [[ "$tty" != "not a tty" ]] && sleep 3 && echo "DAEMON OUT" >$tty

exit # This may never run. Why is it here then? It's pretty.
     # Kind of like, "The End" at the end of a movie that you
     # already know is over. It's always nice.
